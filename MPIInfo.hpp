#pragma once

#include <mpi.h>

#include "datatypes.hpp"

struct MPIInfo
{
    MPI_Comm comm;
    int rank;

    int numProcesses;
    std::array<int, 2> processesPerDim;
    std::array<int, 2> coord;
    std::array<int, 4> neighborRanks;

    MPIInfo(const std::array<int, 2>& processesPerDimArg) : processesPerDim(processesPerDimArg)
    {
        numProcesses = processesPerDim[COORD_X] * processesPerDim[COORD_Y];
        int usePeriods[2] = {0, 0};

        int worldSize;
        MPI_Comm_size(MPI_COMM_WORLD, &worldSize);

        // The number of MPI processes must fill the topology
        if (worldSize != numProcesses)
        {
            std::cout << "Error: The number of MPI processes (" << worldSize
                      << ") doesn't match the topology size (" << numProcesses << ")." << std::endl;
            exit(EXIT_FAILURE);
        }

        // Create a carthesian communicator
        MPI_Cart_create(MPI_COMM_WORLD, 2, processesPerDim.data(), usePeriods, 1, &comm);

        // Update the rank to be relevant to the new communicator
        MPI_Comm_rank(comm, &rank);

        // Obtain the 2D coordinates in the new communicator
        MPI_Cart_coords(comm, rank, 2, coord.data());

        // Obtain the direct neighbor ranks
        MPI_Cart_shift(comm, 0, 1, &neighborRanks[DIR_LEFT], &neighborRanks[DIR_RIGHT]);
        MPI_Cart_shift(comm, 1, 1, &neighborRanks[DIR_BOTTOM], &neighborRanks[DIR_TOP]);
    }

    KOKKOS_INLINE_FUNCTION
    bool isRoot() const { return rank == 0; }
};