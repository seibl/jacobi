#pragma once

#include <iostream>

#include "Config.hpp"

class JacobiKokkos
{
public:
    JacobiKokkos(int nx, int ny) : nx_(nx), ny_(ny), old_("old", nx, ny), new_("new", nx, ny)
    {
        mpiBuffer_[BUFFER_SEND][DIR_LEFT] = MPIBuffer("send_left", ny);
        mpiBuffer_[BUFFER_SEND][DIR_TOP] = MPIBuffer("send_top", nx);
        mpiBuffer_[BUFFER_SEND][DIR_RIGHT] = MPIBuffer("send_right", ny);
        mpiBuffer_[BUFFER_SEND][DIR_BOTTOM] = MPIBuffer("send_bottom", nx);

        mpiBuffer_[BUFFER_RECV][DIR_LEFT] = MPIBuffer("recv_left", ny);
        mpiBuffer_[BUFFER_RECV][DIR_TOP] = MPIBuffer("recv_top", nx);
        mpiBuffer_[BUFFER_RECV][DIR_RIGHT] = MPIBuffer("recv_right", ny);
        mpiBuffer_[BUFFER_RECV][DIR_BOTTOM] = MPIBuffer("recv_bottom", nx);
    }

    void setBoundaryCondition(const real_t& xOffset, const Config& config)
    {
        auto oldGrid = old_;
        auto newGrid = new_;
        auto ny = ny_;

        auto policy = Kokkos::RangePolicy<>(1, nx_ - 1);
        auto kernel = KOKKOS_LAMBDA(const int idx)
        {
            oldGrid(idx, ny - 1) = boundaryCondition(xOffset + idx * config.gridSpacing[0]);
            newGrid(idx, ny - 1) = boundaryCondition(xOffset + idx * config.gridSpacing[0]);
        };
        Kokkos::parallel_for("setBoundaryCondition", policy, kernel);
        Kokkos::fence();
    }

    real_t iterate()
    {
        auto oldGrid = old_;
        auto newGrid = new_;

        auto sumDelta = 0_r;
        auto policy = Kokkos::MDRangePolicy<Kokkos::Rank<2>>({1, 1}, {nx_ - 1, ny_ - 1});
        auto kernel = KOKKOS_LAMBDA(const int idx, const int idy, real_t& delta)
        {
            newGrid(idx, idy) = 0.25_r * (oldGrid(idx - 1, idy) + oldGrid(idx + 1, idy) +
                                          oldGrid(idx, idy - 1) + oldGrid(idx, idy + 1));
            delta += std::abs(newGrid(idx, idy) - oldGrid(idx, idy));
        };
        Kokkos::parallel_reduce("iteration", policy, kernel, sumDelta);
        Kokkos::fence();
        std::swap(old_, new_);
        return sumDelta;
    }

    void communicateHalo(const MPIInfo& mpiInfo)
    {
        std::array<MPI_Status, 4> status;
        gatherTopBottom();
        if (mpiInfo.neighborRanks[DIR_TOP] != MPI_PROC_NULL)
        {
            MPI_Sendrecv(mpiBuffer_[BUFFER_SEND][DIR_TOP].data(),
                         mpiBuffer_[BUFFER_SEND][DIR_TOP].size(),
                         MPI_DOUBLE,
                         mpiInfo.neighborRanks[DIR_TOP],
                         DIR_TOP,
                         mpiBuffer_[BUFFER_RECV][DIR_TOP].data(),
                         mpiBuffer_[BUFFER_RECV][DIR_TOP].size(),
                         MPI_DOUBLE,
                         mpiInfo.neighborRanks[DIR_TOP],
                         DIR_BOTTOM,
                         mpiInfo.comm,
                         &status[DIR_TOP]);
        }
        if (mpiInfo.neighborRanks[DIR_BOTTOM] != MPI_PROC_NULL)
        {
            MPI_Sendrecv(mpiBuffer_[BUFFER_SEND][DIR_BOTTOM].data(),
                         mpiBuffer_[BUFFER_SEND][DIR_BOTTOM].size(),
                         MPI_DOUBLE,
                         mpiInfo.neighborRanks[DIR_BOTTOM],
                         DIR_BOTTOM,
                         mpiBuffer_[BUFFER_RECV][DIR_BOTTOM].data(),
                         mpiBuffer_[BUFFER_RECV][DIR_BOTTOM].size(),
                         MPI_DOUBLE,
                         mpiInfo.neighborRanks[DIR_BOTTOM],
                         DIR_TOP,
                         mpiInfo.comm,
                         &status[DIR_BOTTOM]);
        }
        scatterTopBottom(mpiInfo);

        gatherLeftRight();
        if (mpiInfo.neighborRanks[DIR_RIGHT] != MPI_PROC_NULL)
        {
            MPI_Sendrecv(mpiBuffer_[BUFFER_SEND][DIR_RIGHT].data(),
                         mpiBuffer_[BUFFER_SEND][DIR_RIGHT].size(),
                         MPI_DOUBLE,
                         mpiInfo.neighborRanks[DIR_RIGHT],
                         DIR_RIGHT,
                         mpiBuffer_[BUFFER_RECV][DIR_RIGHT].data(),
                         mpiBuffer_[BUFFER_RECV][DIR_RIGHT].size(),
                         MPI_DOUBLE,
                         mpiInfo.neighborRanks[DIR_RIGHT],
                         DIR_LEFT,
                         mpiInfo.comm,
                         &status[DIR_RIGHT]);
        }
        if (mpiInfo.neighborRanks[DIR_LEFT] != MPI_PROC_NULL)
        {
            MPI_Sendrecv(mpiBuffer_[BUFFER_SEND][DIR_LEFT].data(),
                         mpiBuffer_[BUFFER_SEND][DIR_LEFT].size(),
                         MPI_DOUBLE,
                         mpiInfo.neighborRanks[DIR_LEFT],
                         DIR_LEFT,
                         mpiBuffer_[BUFFER_RECV][DIR_LEFT].data(),
                         mpiBuffer_[BUFFER_RECV][DIR_LEFT].size(),
                         MPI_DOUBLE,
                         mpiInfo.neighborRanks[DIR_LEFT],
                         DIR_RIGHT,
                         mpiInfo.comm,
                         &status[DIR_LEFT]);
        }
        scatterLeftRight(mpiInfo);
    }

    void gatherTopBottom()
    {
        auto mpiBufferSendTop = mpiBuffer_[BUFFER_SEND][DIR_TOP];
        auto mpiBufferSendBottom = mpiBuffer_[BUFFER_SEND][DIR_BOTTOM];
        auto old = old_;
        auto nx = nx_;
        auto ny = ny_;

        auto policy = Kokkos::RangePolicy<>(0, nx);
        auto kernel = KOKKOS_LAMBDA(const int idx)
        {
            mpiBufferSendBottom(idx) = old(idx, 1);
            mpiBufferSendTop(idx) = old(idx, ny - 2);
        };
        Kokkos::parallel_for("gatherTopBottom", policy, kernel);
        Kokkos::fence();
    }

    void scatterTopBottom(const MPIInfo& mpiInfo)
    {
        auto mpiBufferRecvTop = mpiBuffer_[BUFFER_RECV][DIR_TOP];
        auto mpiBufferRecvBottom = mpiBuffer_[BUFFER_RECV][DIR_BOTTOM];
        auto old = old_;
        auto nx = nx_;
        auto ny = ny_;

        auto policy = Kokkos::RangePolicy<>(0, nx);
        auto kernel = KOKKOS_LAMBDA(const int idx)
        {
            if (mpiInfo.neighborRanks[DIR_BOTTOM] != MPI_PROC_NULL)
                old(idx, 0) = mpiBufferRecvBottom(idx);
            if (mpiInfo.neighborRanks[DIR_TOP] != MPI_PROC_NULL)
                old(idx, ny - 1) = mpiBufferRecvTop(idx);
        };
        Kokkos::parallel_for("scatterTopBottom", policy, kernel);
        Kokkos::fence();
    }

    void gatherLeftRight()
    {
        auto mpiBufferSendLeft = mpiBuffer_[BUFFER_SEND][DIR_LEFT];
        auto mpiBufferSendRight = mpiBuffer_[BUFFER_SEND][DIR_RIGHT];
        auto old = old_;
        auto nx = nx_;
        auto ny = ny_;

        auto policy = Kokkos::RangePolicy<>(0, ny);
        auto kernel = KOKKOS_LAMBDA(const int idy)
        {
            mpiBufferSendLeft(idy) = old(1, idy);
            mpiBufferSendRight(idy) = old(nx - 2, idy);
        };
        Kokkos::parallel_for("gatherLeftRight", policy, kernel);
        Kokkos::fence();
    }

    void scatterLeftRight(const MPIInfo& mpiInfo)
    {
        auto mpiBufferRecvLeft = mpiBuffer_[BUFFER_RECV][DIR_LEFT];
        auto mpiBufferRecvRight = mpiBuffer_[BUFFER_RECV][DIR_RIGHT];
        auto old = old_;
        auto nx = nx_;
        auto ny = ny_;

        auto policy = Kokkos::RangePolicy<>(0, ny);
        auto kernel = KOKKOS_LAMBDA(const int idy)
        {
            if (mpiInfo.neighborRanks[DIR_LEFT] != MPI_PROC_NULL)
                old(0, idy) = mpiBufferRecvLeft(idy);
            if (mpiInfo.neighborRanks[DIR_RIGHT] != MPI_PROC_NULL)
                old(nx - 1, idy) = mpiBufferRecvRight(idy);
        };
        Kokkos::parallel_for("scatterLeftRight", policy, kernel);
        Kokkos::fence();
    }

private:
    ScalarView old_;
    ScalarView new_;

    std::array<std::array<MPIBuffer, 4>, 2> mpiBuffer_;

    int nx_;
    int ny_;
};