#pragma once

#include <Kokkos_Core.hpp>

#include "datatypes.hpp"

struct Config
{
    static constexpr Point minCorner = {0_r, 0_r};
    static constexpr Point maxCorner = {1_r, 1_r};
    static constexpr Point diameter = {maxCorner[0] - minCorner[0], maxCorner[1] - minCorner[1]};

    std::array<int, 2> processesPerDim = {1, 1};
    std::array<int, 2> nodesPerDimPerProcess = {4000, 4000};

    std::array<int, 2> numGridNodes;
    std::array<real_t, 2> gridSpacing;

    int iterations = 10;
    int outputInterval = 100;

    void init()
    {
        numGridNodes[COORD_X] = processesPerDim[COORD_X] * nodesPerDimPerProcess[COORD_X] + 2;
        numGridNodes[COORD_Y] = processesPerDim[COORD_Y] * nodesPerDimPerProcess[COORD_Y] + 2;
        gridSpacing[0] = (maxCorner[0] - minCorner[0]) / numGridNodes[0];
        gridSpacing[1] = (maxCorner[1] - minCorner[1]) / numGridNodes[1];
    }

    KOKKOS_INLINE_FUNCTION
    Point nodeToPoint(const Node& node) const
    {
        assert(node[0] >= 0);
        assert(node[1] >= 0);

        assert(node[0] < numGridNodes[0]);
        assert(node[1] < numGridNodes[1]);

        return {node[0] * gridSpacing[0] + minCorner[0], node[1] * gridSpacing[1] + minCorner[1]};
    }

    KOKKOS_INLINE_FUNCTION
    Node pointToNode(const Point& point) const
    {
        assert(point[0] >= minCorner[0]);
        assert(point[1] >= minCorner[1]);

        assert(point[0] < maxCorner[0]);
        assert(point[1] < maxCorner[1]);

        auto result = Node{int_c((point[0] - minCorner[0]) / gridSpacing[0]),
                           int_c((point[1] - minCorner[1]) / gridSpacing[1])};

        result[0] = std::max(0, result[0]);
        result[1] = std::max(0, result[1]);

        result[0] = std::min(numGridNodes[0] - 1, result[0]);
        result[1] = std::min(numGridNodes[1] - 1, result[1]);

        return result;
    }
};

KOKKOS_INLINE_FUNCTION
real_t boundaryCondition(const real_t& x) { return std::sin(3.14_r * x / Config::diameter[0]); }

KOKKOS_INLINE_FUNCTION
real_t analyticSolution(const Point& pt)
{
    return std::sin(3.14_r * pt[0] / Config::diameter[0]) *
           std::sinh(3.14_r * pt[1] / Config::diameter[0]);
}