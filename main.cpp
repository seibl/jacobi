#include <mpi.h>

#include <CLI/App.hpp>
#include <CLI/Config.hpp>
#include <CLI/Formatter.hpp>
#include <Kokkos_Core.hpp>
#include <iostream>

#include "MPIInfo.hpp"
#include "jacobi.hpp"

void runJacobi(const MPIInfo& mpiInfo, const Config& config)
{
    JacobiKokkos jacobi(config.nodesPerDimPerProcess[COORD_X] + 2,
                        config.nodesPerDimPerProcess[COORD_Y] + 2);
    if (mpiInfo.coord[COORD_Y] == mpiInfo.processesPerDim[COORD_Y] - 1)
        jacobi.setBoundaryCondition(
            config.minCorner[COORD_X] + mpiInfo.coord[COORD_X] * config.diameter[COORD_X] /
                                            mpiInfo.processesPerDim[COORD_X],
            config);
    MPI_Barrier(mpiInfo.comm);
    Kokkos::Timer timer;
    for (auto i = 0; i < config.iterations; ++i)
    {
        auto deltaLocal = jacobi.iterate();
        if (i % config.outputInterval == 0)
        {
            real_t deltaGlobal;
            MPI_Allreduce(&deltaLocal, &deltaGlobal, 1, MPI_DOUBLE, MPI_SUM, mpiInfo.comm);
            if (mpiInfo.isRoot()) std::cout << deltaGlobal << std::endl;
        }
        jacobi.communicateHalo(mpiInfo);
    }
    MPI_Barrier(mpiInfo.comm);
    auto seconds = timer.seconds();
    if (mpiInfo.isRoot())
    {
        std::cout << "runtime: " << seconds << "s" << std::endl;
    }
}

int main(int argc, char** argv)
{
    MPI_Init(&argc, &argv);
    Kokkos::initialize(argc, argv);

    Config config;

    CLI::App app{"Jacobi Solver for Laplace Equation. Weak Scaling."};
    app.add_option("pX", config.processesPerDim[COORD_X], "Number of processes in X direction.");
    app.add_option("pY", config.processesPerDim[COORD_Y], "Number of processes in Y direction.");
    app.add_option("--nX",
                   config.nodesPerDimPerProcess[COORD_X],
                   "Number of grid points per processes in X direction.");
    app.add_option("--nY",
                   config.nodesPerDimPerProcess[COORD_Y],
                   "Number of grid points per processes in Y direction.");
    app.add_option("--iterations", config.iterations, "Number of Jacobi iterations.");
    app.add_option("--outputInterval", config.outputInterval, "Delta is printed every X steps.");
    CLI11_PARSE(app, argc, argv);

    config.init();

    MPIInfo mpiInfo(config.processesPerDim);

    if (mpiInfo.isRoot())
    {
        std::cout << "execution space: " << typeid(Kokkos::DefaultExecutionSpace).name()
                  << std::endl;
        std::cout << "memory space: " << typeid(Kokkos::DefaultExecutionSpace::memory_space).name()
                  << std::endl;
    }

    runJacobi(mpiInfo, config);

    Kokkos::finalize();
    MPI_Finalize();
    return EXIT_SUCCESS;
}
